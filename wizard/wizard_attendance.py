from trytond.model import ModelView, fields
from datetime import datetime
from trytond.wizard import Wizard, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['EmployeeAttendanceSignIn', 'EmployeeSignInWizard',
           'EmployeeAttendanceSignOut', 'EmployeeSignOutWizard']


class EmployeeAttendanceSignIn(ModelView):
    'Employee Attendance'
    __name__ = 'hr.employee_attendance_signin'

    employee = fields.Many2One(
        'company.employee', 'Employee', required=True, select=True,
        readonly=True
    )

    sign_in_time = fields.DateTime('Sign In Time')

    @staticmethod
    def default_sign_in_time():
        return datetime.now()

    @staticmethod
    def default_employee():
        User = Pool().get('res.user')
        employee_id = None

        user = User(Transaction().user)

        if user.employee:
            employee_id = user.employee.id
        return employee_id


class EmployeeSignInWizard(Wizard):
    'Employee Attendance Sign In Wizard'
    __name__ = 'wizard.employee.attendance.signin'

    start_state = 'signin'

    signin = StateView(
        'hr.employee_attendance_signin',
        'hr.form_employee_attendance_sign_in', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Sign In', 'sign_in', 'tryton-ok', default=True),
            ])

    sign_in = StateAction('hr.act_attendance_list')

    def transition_sign_in(self):
        Attendance = Pool().get('employee.attendance')
        employee_id = self.signin.employee.id

        if Attendance.check_signed_in(employee_id)[0]:
            self.raise_user_error("The employee is already Signed In")

        Attendance.create([{
            'employee': employee_id,
            'in_time': self.signin.sign_in_time,
        }])
        return 'end'


class EmployeeAttendanceSignOut(ModelView):
    'Employee Attendance Sign Out'
    __name__ = 'hr.employee_attendance_signout'

    employee = fields.Many2One(
        'company.employee', 'Employee', required=True, select=True,
        readonly=True
    )

    sign_out_time = fields.DateTime('Sign Out Time', readonly=True)

    @staticmethod
    def default_sign_out_time():
        return datetime.now()

    @staticmethod
    def default_employee():
        User = Pool().get('res.user')
        employee_id = None
        user = User(Transaction().user)
        if user.employee:
            employee_id = user.employee.id
        return employee_id


class EmployeeSignOutWizard(Wizard):
    'Employee Attendance Sign out Wizard'
    __name__ = 'wizard.employee.attendance.signout'

    start_state = 'signout'

    signout = StateView(
        'hr.employee_attendance_signout',
        'hr.form_employee_attendance_sign_out', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Sign Out', 'sign_out', 'tryton-ok', default=True),
            ])

    sign_out = StateAction('hr.act_attendance_list')

    def transition_sign_out(self):
        Attendance = Pool().get('employee.attendance')
        res, rec = Attendance.check_signed_in(self.signout.employee.id)
        in_time = Attendance.read(rec, ['in_time'])
        in_time = in_time[0]['in_time']
        hours = (self.signout.sign_out_time - in_time).total_seconds()/3600
        if res:
            Attendance.write(rec, {
                'out_time': self.signout.sign_out_time,
                'hours': format(hours, '.2f'),
            })
        return 'end'
