from sql import Literal
from sql.aggregate import Max, Sum
from sql.conditionals import Coalesce
from sql.functions import Extract
from datetime import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, PYSONEncoder, Date, Get
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['HoursEmployeeWeekly', 'HoursEmployeeMonthly'] 

class HoursEmployeeWeekly(ModelSQL, ModelView):
    'Hours per Employee per Week'
    __name__ = 'hr.hours_employee_weekly'
    year = fields.Char('Year', select=True)
    week = fields.Integer('Week', select=True)
    employee = fields.Many2One('company.employee', 'Employee', select=True)
    hours = fields.Float('Hours', required=True)

    @classmethod
    def __setup__(cls):
        super(HoursEmployeeWeekly, cls).__setup__()
        cls._order.insert(0, ('year', 'DESC'))
        cls._order.insert(1, ('week', 'DESC'))
        cls._order.insert(2, ('employee', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        Attendance = pool.get('employee.attendance')
        attendance = Attendance.__table__()
        
        type_name = cls.year.sql_type().base
        
        year_column = Extract('YEAR', attendance.date).cast(type_name).as_('year')
        week_column = Extract('WEEK', attendance.date).as_('week')
        return attendance.select(
            Max(Extract('WEEK', attendance.date)
                + Extract('YEAR', attendance.date) * 100
                + attendance.employee * 1000000).as_('id'),
            Max(attendance.create_uid).as_('create_uid'),
            Max(attendance.create_date).as_('create_date'),
            Max(attendance.write_uid).as_('write_uid'),
            Max(attendance.write_date).as_('write_date'),
            year_column,
            week_column,
            attendance.employee,
            Sum(Coalesce(attendance.hours, 0)).as_('hours'),
            group_by=(year_column, week_column, attendance.employee))
            
class HoursEmployeeMonthly(ModelSQL, ModelView):
    'Hours per Employee per Month'
    __name__ = 'hr.hours_employee_monthly'
    year = fields.Char('Year', select=True)
    month = fields.Integer('Month', select=True)
    employee = fields.Many2One('company.employee', 'Employee', select=True)
    hours = fields.Float('Hours', required=True)

    @classmethod
    def __setup__(cls):
        super(HoursEmployeeMonthly, cls).__setup__()
        cls._order.insert(0, ('year', 'DESC'))
        cls._order.insert(1, ('month', 'DESC'))
        cls._order.insert(2, ('employee', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        Attendance = pool.get('employee.attendance')
        attendance = Attendance.__table__()
        
        type_name = cls.year.sql_type().base
        
        
        
        year_column = Extract('YEAR', attendance.date).cast(type_name).as_('year')
        month_column = Extract('MONTH', attendance.date).as_('month')
        return attendance.select(
            Max(Extract('MONTH', attendance.date)
                + Extract('YEAR', attendance.date) * 100
                + attendance.employee * 1000000).as_('id'),
            Max(attendance.create_uid).as_('create_uid'),
            Max(attendance.create_date).as_('create_date'),
            Max(attendance.write_uid).as_('write_uid'),
            Max(attendance.write_date).as_('write_date'),
            year_column,
            month_column,
            attendance.employee,
            Sum(Coalesce(attendance.hours, 0)).as_('hours'),
            group_by=(year_column, month_column, attendance.employee))            
